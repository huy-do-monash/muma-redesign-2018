# MUMA Redesign - 2019 - Web

#### Status: Ongoing

- Design: https://drive.google.com/file/d/1MAFn-cos8Xzf2CbkHuuzyB5GS_XPY0Jc/view?usp=sharing
- Fonts: HelveticaNeue

## Getting Started

1. [Requirements](##requirements)
1. [Installation](##getting-started)
1. [Running the Project](##running-the-project)
1. [Live Development](##local-development)
   - [Hot Reloading](###hot-reloading)
1. [Routing](##routing)
1. [Testing](##testing)
1. [Deployment](##deployment)

## Requirements

- node `^10.0.0`
- npm `>=6.5.0`

## Installation

After confirming that your environment meets the above [requirements](#requirements), you can clone via the following:

```bash
$ git clone https://bitbucket.apps.monash.edu:8443/scm/easc/muma-redesign-2018.git
$ cd muma-redesign
$ npm install  # Install project dependencies
```

- CORS plugin for local development.

## Running the Project

After completing the [installation](#installation) step, you're ready to start the project.

```bash
$ npm start  # Start the development server
```

Default location
[http://localhost:8000](http://localhost:8000)

While developing, you will probably rely mostly on `npm start`; however, there are additional scripts at your disposal: **Work-in-Progress**

| `npm <script>` | Description                                                                                                         |
| -------------- | ------------------------------------------------------------------------------------------------------------------- |
| `start`        | Serves your app at `localhost:8000`                                                                                 |
| `lint:js`      | [Lints](http://stackoverflow.com/questions/8503559/what-is-linting) the project's JavaScript for potential errors   |
| `lint:styles`  | Lints the project's stylesheets for potential errors                                                                |
| `build:dist`   | Build the project. **Important** to run before pushing **master**, some files are used by monash.edu/muma-redesign! |

If you wish to see the full list of all the commands, you can refer to package.json

## Deploying the Project

The project is deployed automatically via a Git Bridge Asset on Squiz Matrix via the **master** branch.

## Live Development

### Hot Reloading

Hot reloading is enabled by default when the application is running in development mode (`npm start`). This feature is implemented with webpack's [Hot Module Replacement](https://webpack.github.io/docs/hot-module-replacement.html) capabilities, where code updates can be injected to the application while it's running, no full reload required. Here's how it works:  
 e

- For **JavaScript** modules, a code change will trigger the application to re-render from the top of the tree.

- For **Sass**, any change will update the styles in realtime, no additional configuration or reload needed.

## Testing

**Work in Progress**
