const path = require('path');

const config = require('./site.config');
const loaders = require('./webpack.loaders');
const plugins = require('./webpack.plugins');
/* eslint-disable */
function recursiveIssuer(m) {
  if (m.issuer) {
    return recursiveIssuer(m.issuer);
  } else if (m.name) {
    return m.name;
  } else {
    return false;
  }
}
/* eslint-enable */
module.exports = {
  context: path.join(config.root, config.paths.src),
  entry: {
    main: '../src/javascripts/globals-script.js',
    itemStacker: '../src/javascripts/item-stacker.js',
    searchPage: '../src/javascripts/search-page.js',
    simplebar: '../src/javascripts/simplebar.js',
    collection: '../src/javascripts/collection-browser.js',
    collectionDEV: '../src/javascripts/collection-browser-dev.js',
    collectionQA: '../src/javascripts/collection-browser-qa.js',
    collectionFB: '../src/javascripts/collection-browser-fb.js',
    collectionFBc: '../src/javascripts/collection-browser-fb-custom.js',
    itemstacker: '../src/javascripts/item-stacker.js',
    styles: '../src/stylesheets/styles.scss',
  },

  output: {
    path: path.join(config.root, config.paths.dist),
    filename: '[name].js',
  },
  mode: ['production', 'development'].includes(config.env)
    ? config.env
    : 'development',
  devtool: 'cheap-eval-source-map',
  devServer: {
    contentBase: path.join(config.root, config.paths.src),
    watchContentBase: true,
    hot: true,
    open: false,
    port: config.port,
    host: config.dev_host,
  },
  module: {
    rules: loaders,
  },
  plugins,
};
