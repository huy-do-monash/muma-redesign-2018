module.exports = {
  extends: 'stylelint-prettier',
  plugins: ['stylelint-prettier'],
  rules: {
    'prettier/prettier': true,
  },
};
