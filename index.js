document.addEventListener('DOMContentLoaded', () => {
  // console.log('DOM fully loaded and parsed');
  document.querySelector('.header-container__menu').onclick = () => {
    // console.log('Clicked the menu!');
    document
      .querySelector('.main-navigation-menu__wrapper')
      .classList.toggle('main-navigation-menu__wrapper-active');
    document.body.classList.toggle('background-grey');
  };
});
