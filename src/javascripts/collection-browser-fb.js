import { initItemStacker, imageLoadObserver } from './item-stacker';
import '../stylesheets/collection-browser.scss';
import itemViewer from './itemViewer';

const axios = require('axios');

const queryURL =
  'https://www.monash.edu/muma/_designs/browse-collection/funnelback';

const extraQueryValue =
  document.querySelector('.collection-browser__extra-query').textContent || '';

function isIE() {
  const ua = window.navigator.userAgent; // Check the userAgent property of the window.navigator object
  const msie = ua.indexOf('MSIE '); // IE 10 or older
  const trident = ua.indexOf('Trident/'); // IE 11

  return msie > 0 || trident > 0;
}

function sanitiseString(str) {
  return str
    .trim()
    .replace(/\s/g, '_')
    .replace(/[^\w_...\-'"]/gi, '');
}

// ES6
function debounced(delay, fn) {
  let timerId;
  return function(...args) {
    if (timerId) {
      clearTimeout(timerId);
    }
    timerId = setTimeout(() => {
      fn(...args);
      timerId = null;
    }, delay);
  };
}

// This function needs optimisation
function updateFilterOptions() {
  // Get the wrappers for all checkboxes
  const optionGroupWrappers = {
    artist: document.querySelector(
      '.collection-browser__select-options[data-filter-option="artist"]',
    ),
    year: document.querySelector(
      '.collection-browser__select-options[data-filter-option="year"]',
    ),
    medium: document.querySelector(
      '.collection-browser__select-options[data-filter-option="medium"]',
    ),
    subject: document.querySelector(
      '.collection-browser__select-options[data-filter-option="subject"]',
    ),
  };

  // Disable the dropdown button if there are no checkboxes in any filter field
  Object.keys(optionGroupWrappers).forEach(key => {
    if (
      optionGroupWrappers[key].querySelectorAll(
        '.collection-browser__select-options--checkbox',
      ).length < 1
    ) {
      document
        .querySelector(
          `.collection-browser__dropdown-button[data-filter-option="${key}"]`,
        )
        .setAttribute('disabled', 'disabled');
    } else {
      document
        .querySelector(
          `.collection-browser__dropdown-button[data-filter-option="${key}"]`,
        )
        .removeAttribute('disabled');
    }
  });

  // Add all checkboxes into an object as arrays according to each filter
  const optionGroupElements = {
    artist: Array.prototype.slice.call(
      optionGroupWrappers.artist.querySelectorAll(
        '.collection-browser__select-options--checkbox',
      ),
    ),
    year: Array.prototype.slice.call(
      optionGroupWrappers.year.querySelectorAll(
        '.collection-browser__select-options--checkbox',
      ),
    ),
    medium: Array.prototype.slice.call(
      optionGroupWrappers.medium.querySelectorAll(
        '.collection-browser__select-options--checkbox',
      ),
    ),
    subject: Array.prototype.slice.call(
      optionGroupWrappers.subject.querySelectorAll(
        '.collection-browser__select-options--checkbox',
      ),
    ),
  };

  // Create a new element for the checkbox for each of the new value
  Object.keys(optionGroupElements).forEach(key => {
    // Check and filter out any duplicate values in the filters
    const existingValues = [];
    optionGroupElements[key] = optionGroupElements[key].filter(item => {
      if (
        existingValues.indexOf(item.querySelector('input').dataset.value) < 0
      ) {
        existingValues.push(item.querySelector('input').dataset.value);
        return true;
      }
      return false;
    });

    optionGroupWrappers[key].querySelector(
      '.collection-browser__select-options--container',
    ).innerHTML = '';

    // Sort each of the option groups
    if (key === 'year') {
      optionGroupElements[key].sort((a, b) => {
        let valueA = a.querySelector('input').dataset.value;
        let valueB = b.querySelector('input').dataset.value;

        valueA = valueA.replace(/\D/g, '').slice(0, 4);
        valueB = valueB.replace(/\D/g, '').slice(0, 4);
        if (!Number.isNaN(valueA - valueB)) return valueA - valueB;
        if (valueA > valueB) return 1;
        if (valueA < valueB) return -1;
        return 0;
      });
      optionGroupElements[key].push(optionGroupElements[key].shift());
    } else if (key === 'artist') {
      optionGroupElements[key].sort((a, b) => {
        const valueA = a.querySelector('input').dataset.value;
        const valueB = b.querySelector('input').dataset.value;

        const surnameA = valueA
          .trim()
          .split(' ')
          .splice(-1)[0];
        const surnameB = valueB
          .trim()
          .split(' ')
          .splice(-1)[0];
        return surnameA.localeCompare(surnameB, 'en', {
          sensitivity: 'base',
        });
      });
    } else {
      optionGroupElements[key].sort((a, b) => {
        const valueA = a.querySelector('input').dataset.value;
        const valueB = b.querySelector('input').dataset.value;
        return valueA.localeCompare(valueB, 'en', { sensitivity: 'base' });
      });
    }

    // Replace the existing checkbox lists with the new one
    optionGroupElements[key].forEach(element => {
      optionGroupWrappers[key]
        .querySelector('.collection-browser__select-options--container')
        .appendChild(element);
    });
  });
}

function handleFilterButton(event) {
  let filterList;
  const filterButton = event.target;
  if (filterButton.classList.contains('active')) {
    // Expanded Filter
    filterList = document.querySelector(
      '.collection-browser__select-options.active',
    );
    // If a filter is active, deactive both the button and the filter field
    // console.log('This filter is active. Removing it');
    filterButton.classList.remove('active');
    filterButton.setAttribute('aria-pressed', 'false');
    // Also deactivating the relevant filter
    filterList.classList.remove('active');
    filterList.setAttribute('aria-expanded', 'false');
  } else {
    // console.log('This filter is NOT active. Adding it');
    // Relevant Filter
    filterList = document.querySelector(
      `.collection-browser__select-options[data-filter-option=${filterButton.dataset.filterOption}]`,
    );
    /* 
      There's no guarantee that there are other Filters active
      so we need to check if first they are active before removing the
      active class
      */
    const activeBtn = document.querySelector(
      '.collection-browser__dropdown-button button.active',
    );
    const activeFilter = document.querySelector(
      '.collection-browser__select-options.active',
    );

    if (activeBtn) {
      activeBtn.classList.remove('active');
      activeBtn.setAttribute('aria-pressed', 'false');
    }
    if (activeFilter) {
      activeFilter.classList.remove('active');
      activeFilter.setAttribute('aria-expanded', 'false');
    }
    filterButton.classList.add('active');
    filterButton.setAttribute('aria-pressed', 'true');

    filterList.classList.add('active');
    filterList.setAttribute('aria-expanded', 'false');
  }
}

function handlefilterCheckbox(event) {
  const filterCheckbox = event.target;
  const selectedOptions = filterCheckbox
    .closest('.collection-browser__select-options')
    .querySelector('input.collection-browser__select-options--selected');
  if (filterCheckbox.checked) {
    selectedOptions.value += `${filterCheckbox.dataset.value},`;
  } else {
    selectedOptions.value = selectedOptions.value.replace(
      `${filterCheckbox.dataset.value},`,
      '',
    );
  }
  debouncedSearchEvent(); // eslint-disable-line
  // debouncedSearchEvent;
}

function initFilters() {
  // Find all buttons for the Filter (Artist, Year, Medium and Subject)
  const filterButtons = document.querySelectorAll(
    '.collection-browser__dropdown-button button',
  );
  /* Add an EventListener to check for click
   * If the button is clicked, reveal the selection panel and deactivate all other active filters
   * If an active button is clicked, deactivate it
   */
  filterButtons.forEach(filterButton =>
    filterButton.addEventListener('click', handleFilterButton),
  );

  // Handle the the selection of the checkboxes
  const filterCheckboxes = document.querySelectorAll(
    ".collection-browser__select-options .collection-browser__select-options--container input[type='checkbox']",
  );
  filterCheckboxes.forEach(filterCheckbox => {
    filterCheckbox.addEventListener('change', handlefilterCheckbox);
  });

  document
    .querySelector('.collection-browser__clear-button')
    .addEventListener('click', () => {
      filterCheckboxes.forEach(filterCheckbox => {
        filterCheckbox.checked = false; // eslint-disable-line no-param-reassign
        filterCheckbox.dispatchEvent(new Event('change'));
      });
      document.querySelector(
        '.collection-browser__select--applied-filters',
      ).textContent = '';
      document.querySelector('.collection-browser__search-button').click();
    });
}

function createQuery(resultNo = 0) {
  const keywords = document.querySelector('.collection-browser__search-field');
  const year = document.querySelector(
    ".collection-browser__select-options--selected[data-filter-option='year']",
  );
  const artist = document.querySelector(
    ".collection-browser__select-options--selected[data-filter-option='artist']",
  );
  const medium = document.querySelector(
    ".collection-browser__select-options--selected[data-filter-option='medium']",
  );
  const subject = document.querySelector(
    ".collection-browser__select-options--selected[data-filter-option='subject']",
  );

  let keywordQuery = '';
  let facetQuery = '';
  let offset = 0;
  let appliedFilters = '';
  let extraQuery = '';

  if (keywords) {
    if (keywords.value.length > 0) {
      keywordQuery = `${encodeURIComponent(keywords.value)}`;
      keywordQuery = `keyword_query=${btoa(keywordQuery)}&`;
    }
  }

  if (year) {
    if (year.value.length > 0) {
      const years = year.value.slice(0, -1).split(',');
      years.forEach(item => {
        // let displayText = '';
        // if (item.toLowerCase() === 'indeterminable') {
        //   displayText = 'No date';
        //   facetQuery += 'meta_date=!padrenull&';
        // } else {
        //   const yearStart = item.split('-')[0];
        //   const yearEnd = item.split('-')[0];

        //   facetQuery += `ge_date=${yearStart}&le_date=${yearEnd}&`;

        //   if (yearStart === '0') {
        //     displayText = 'Pre-1960';
        //   } else {
        //     displayText = `${yearStart}s`;
        //   }
        // }
        // appliedFilters += `${displayText}, `;
        facetQuery += `f.scoped+date%7C${item
          .replace('Unknown+date', 'nodate')
          .replace('Pre+1960s', 'pre1960')}=${item}&`;
        appliedFilters += `${item.replace(/\b(\w)/g, s => s.toUpperCase())}, `;
      });
    }
  }

  if (artist) {
    if (artist.value.length > 0) {
      const artists = artist.value.slice(0, -1).split(',');

      artists.forEach(item => {
        facetQuery += `f.artist%7CpersonName=${item.replace(/\s/g, '+')}&`;
        appliedFilters += `${item.replace(/\b(\w)/g, s => s.toUpperCase())}, `;
      });
    }
  }

  if (medium) {
    if (medium.value.length > 0) {
      const mediums = medium.value.slice(0, -1).split(',');

      mediums.forEach(item => {
        facetQuery += `f.material%7Cmaterial=${item.replace(' ', '+')}&`;
        appliedFilters += `${item}, `;
      });
    }
  }

  if (subject) {
    if (subject.value.length > 0) {
      const subjects = subject.value.slice(0, -1).split(',');

      subjects.forEach(item => {
        facetQuery += `f.subject%7CsubjectClass=${item.replace(' ', '+')}&`;
        appliedFilters += `${item}, `;
      });
    }
  }

  if (Number.isNaN(resultNo) || Number(resultNo) < 1) {
    offset = `offset=1&`;
  } else {
    offset = `offset=${Number.parseInt(resultNo, 10) + 1}&`;
  }

  // facetQuery += 'meta_status=accessioned';
  facetQuery = `facet_query=${btoa(facetQuery)}`;

  if (appliedFilters.length > 1) {
    // WEBSW-1808
    document.querySelector(
      '.collection-browser__select--applied-filters',
    ).textContent = 'Applied Filters: ';
    document
      .querySelectorAll(
        '.content-container__item.collection-browser__select-options__wrapper input[type=checkbox]:checked',
      )
      .forEach(function(cb) {
        const d = document.createElement('div');
        d.innerText =
          cb.parentElement.textContent || cb.getAttribute('data-value');
        d.className = 'chip';
        const s = document.createElement('button');
        s.className = 'closebtn';
        s.innerHTML = '&times;';
        s.onclick = function() {
          this.parentElement.remove();
          cb.click();
        };
        d.appendChild(s);
        document
          .querySelector('.collection-browser__select--applied-filters')
          .appendChild(d);
      }); // end WEBSW-1808

    // document.querySelector(
    //   '.collection-browser__select--applied-filters',
    // ).textContent = `Applied Filters: ${appliedFilters.slice(0, -2)}`;
    document
      .querySelector('.collection-browser__clear-button')
      .classList.remove('hidden');
  } else {
    document.querySelector(
      '.collection-browser__select--applied-filters',
    ).textContent = '';
    document
      .querySelector('.collection-browser__clear-button')
      .classList.add('hidden');
  }

  // Get extra query paramater from hidden field on page
  if (extraQueryValue.length > 1) {
    extraQuery = `&extra_query=${btoa(extraQueryValue)}`;
  }

  return { keywordQuery, facetQuery, offset, extraQuery };
}

function handleViewMoreButton(event) {
  let offset = 0;
  if (event) {
    // eslint-disable-next-line
    offset = event.target.dataset.offset;
    event.target.remove();
  }

  const queryAll = createQuery(offset);

  // console.log(
  //   'VERNON QUERY (View More)',
  //   `${queryURL}?${queryAll.keywordQuery}${queryAll.offset}${queryAll.facetQuery}&query_type=view_more`,
  // );

  axios
    .get(
      `${queryURL}?${queryAll.keywordQuery}${queryAll.offset}${queryAll.facetQuery}${queryAll.extraQuery}&query_type=view_more`,
    )
    .then(response => {
      console.log(response);
      // reqListener(response.data);
      const responseText = response.data;
      if (document.querySelector('query-error')) {
        document
          .querySelector(
            '#collection-browser__results-container .content-container__item-stacker',
          )
          .insertAdjacentHTML(
            'beforeend',
            `<div class="content-container__item col-12 text-center" style="position:absolute;bottom:-2rem;left:0;right:0;margin:auto;width:9rem;height:3rem;">
                        <p>No more results found</p>
                      </div>`,
            // <div class="content-container__item col-12">
            //   <h5>DEBUG OUTPUT - This section is to be removed before the site can go publicly live - Usually caused by search having no results</h5>
            //   <p>${responseText}</p>
            // </div>,
          );
      } else {
        document
          .querySelector(
            '#collection-browser__results-container .content-container__item-stacker',
          )
          .insertAdjacentHTML('beforeend', responseText);
        initFilters();
        imageLoadObserver();
        initItemStacker();

        const viewMoreBtn = document.querySelector('.js-view-more');
        if (viewMoreBtn) {
          viewMoreBtn.dataset.offset = document.querySelectorAll(
            '.content-container__item-stacker .content-container__inner',
          ).length;
          viewMoreBtn.addEventListener('click', handleViewMoreButton);
        }

        itemViewer();
        // updateFilterOptions();
      }
    })
    .catch(error => console.log('Error', error));
}

function handleSearchButton(event) {
  if (event) {
    event.preventDefault();
    event.target.setAttribute('disabled', 'disabled');
  }

  const queryAll = createQuery();

  console.log('Search Initiated');
  document.getElementById(
    'collection-browser__results-container',
  ).innerHTML = `<div class="spinner">
                  <div class="rect1"></div>
                  <div class="rect2"></div>
                  <div class="rect3"></div>
                  <div class="rect4"></div>
                  <div class="rect5"></div>
                </div>`;
  console.log(
    'VERNON QUERY (Search)',
    `${queryURL}?${queryAll.keywordQuery}${queryAll.offset}${queryAll.facetQuery}${queryAll.extraQuery}`,
  );
  axios
    .get(
      `${queryURL}?${queryAll.keywordQuery}${queryAll.offset}${queryAll.facetQuery}${queryAll.extraQuery}`,
    )
    .then(response => {
      console.log(response);
      const responseText = response.data;
      if (
        responseText.indexOf('Error') > 0 &&
        responseText.indexOf('Error') < 50
      ) {
        document.querySelector(
          '#collection-browser__results-container',
        ).innerHTML = `<div class="content-container__item col-12 text-center">
                        <p>No results found, if this is incorrect, please try again in a few seconds or contact the webmasters for more information</p>
                      </div>`;
        // <div class="content-container__item col-12">
        //   <h5>DEBUG OUTPUT - This section is to be removed before the site can go publicly live</h5>
        //   <p>${responseText}</p>
        // </div>`;
      } else {
        document.querySelector(
          '#collection-browser__results-container',
        ).innerHTML = responseText;
        initFilters();
        imageLoadObserver();
        initItemStacker();

        const viewMoreBtn = document.querySelector('.js-view-more');
        if (viewMoreBtn) {
          viewMoreBtn.dataset.offset = document.querySelectorAll(
            '.content-container__item-stacker .content-container__inner',
          ).length;

          viewMoreBtn.addEventListener('click', handleViewMoreButton);
        }

        itemViewer();
      }
      if (event) event.target.removeAttribute('disabled');
    })
    .catch(error => console.log('Error', error));
}

const debouncedSearchEvent = debounced(1000, handleSearchButton);

function initCollectionBrowser() {
  if (isIE()) {
    if (!Element.prototype.matches) {
      Element.prototype.matches =
        Element.prototype.msMatchesSelector ||
        Element.prototype.webkitMatchesSelector;
    }

    if (!Element.prototype.closest) {
      Element.prototype.closest = function(s) {
        let el = this;
        do {
          if (el.matches(s)) return el;
          el = el.parentElement || el.parentNode;
        } while (el !== null && el.nodeType === 1);
        return null;
      };
    }
  }

  const searchButton = document.querySelector(
    '.collection-browser__search-button',
  );

  const timeFilters = document.querySelector(
    '.collection-browser__filter-main',
  );

  if (searchButton) {
    searchButton.addEventListener('click', handleSearchButton);
  }

  if (timeFilters) {
    timeFilters
      .querySelectorAll('input')
      .forEach(filter => filter.addEventListener('click', handleSearchButton));
  }

  updateFilterOptions();
  setTimeout(handleSearchButton, 2000);
}

document.addEventListener('DOMContentLoaded', initCollectionBrowser);
