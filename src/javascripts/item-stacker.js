let autoResize = true;
const itemWrapOffset = 0;
let initialised = false;

/* Resize Windows Handler
 * This uses requestAnimationFrame, which performs better than setTimeOut
 * Source: https://developer.mozilla.org/en-US/docs/Web/Events/resize
 */
/* eslint-disable */
const optimizedResize = (function() {
  let callbacks = [];
  let running = false;

  // run the actual callbacks
  function runCallbacks() {
    callbacks.forEach(function(callback) {
      callback();
    });

    running = false;
  }

  // fired on resize event
  function resize() {
    if (!running) {
      running = true;

      if (window.requestAnimationFrame) {
        window.requestAnimationFrame(runCallbacks);
      } else {
        setTimeout(runCallbacks, 66);
      }
    }
  }

  // adds callback to loop
  function addCallback(callback) {
    if (callback) {
      callbacks.push(callback);
    }
  }
  return {
    // public method to add additional callback
    add: function(callback) {
      if (!callbacks.length) {
        window.addEventListener('resize', resize);
      }
      addCallback(callback);
    },
  };
})();
/* eslint-enable */
function setStackItemsSize() {
  if (autoResize) {
    const stackContainers = document.querySelectorAll(
      '.content-container__item-stacker:not(.content-container__item-stacker--no-vertical)',
    );
    // eslint-disable-next-line
    stackContainers.forEach(function(stackContainer) {
      const stackItems = stackContainer.querySelectorAll(
        '.content-container__item-stacker .content-container__inner',
      );

      let columnCount = 2;

      if (
        stackContainer.classList.contains(
          'content-container__item-stacker--third',
        )
      ) {
        columnCount = 3;
      } else if (
        stackContainer.classList.contains(
          'content-container__item-stacker--fourth',
        )
      ) {
        columnCount = 4;
      } else if (
        stackContainer.classList.contains(
          'content-container__item-stacker--sixth',
        )
      ) {
        columnCount = 6;
      }
      // eslint-disable-next-line
      let curTop = [0, 0, 0, 0, 0, 0];
      let curLeft = 0;
      let curListingHeight = 0;
      // curPos = 0;

      for (let i = 0; i < stackItems.length; i += 1) {
        let column = (i + 1) % columnCount; // Used to calculate which column a tile is on. 1 being the right-most column.

        if (column <= 0) {
          column = columnCount - 1; // If the modulus returns 0, it's the right-most column
        } else {
          column -= 1; // If the modulus returns higher than 0, minus 1 to get the correct column count for the array
        }

        // Set the positioning styling of each article
        stackItems[i].setAttribute(
          'style',
          `position: absolute; top: ${curTop[column]}px; left: ${curLeft}px;`,
        );
        curTop[column] += stackItems[i].scrollHeight + itemWrapOffset;

        // Set the position for the next article
        if (column + 1 === columnCount) {
          curLeft = 0;
          // curPos = 1;
        } else {
          curLeft += stackItems[i].offsetWidth + itemWrapOffset;
          // curPos += 1;
        }
      }

      for (let i = 0; i < curTop.length; i += 1) {
        if (curTop[i] > curListingHeight) {
          curListingHeight = curTop[i];
        }
      }

      curListingHeight += 10;

      stackContainer.setAttribute('style', `height: ${curListingHeight}px`);
    });
  }
}

/* JS Media Query */
function stackItemsPageHandler(mediaQuery) {
  if (mediaQuery.matches) {
    // If media query matches
    autoResize = false;
    const stackContainers = document.querySelectorAll(
      '.content-container__item-stacker',
    );
    // eslint-disable-next-line
    stackContainers.forEach(function(stackContainer) {
      const stackItems = stackContainer.querySelectorAll(
        '.content-container__item-stacker .content-container__inner',
      );
      for (let i = 0; i < stackItems.length; i += 1) {
        stackItems[i].removeAttribute('style');
      }
      stackContainer.removeAttribute('style');
    });
  } else {
    autoResize = true;
  }
}

function imageLoadObserver() {
  setStackItemsSize();
  document
    .querySelectorAll('img')
    .forEach(image =>
      image.addEventListener('load', () => setStackItemsSize()),
    );
  // console.log('Observer Loaded');
  // const allImages = document.querySelectorAll('img:not([data-loaded]');
  // for (let i = 0; i < allImages.length; i += 1) {
  //   if (allImages[i].complete) {
  //     allImages[i].setAttribute('data-loaded', 'true');
  //   } else {
  //     allImages[i].addEventListener('load', function() {
  //       this.setAttribute('data-loaded', 'true');
  //       setStackItemsSize();
  //     });
  //   }
  // }
}

function initItemStacker() {
  /*
   * UTILITIES
   */

  // let stackItems = document.querySelectorAll(
  //   "#news-events-list .box-listing-element__wrapper"
  // );

  // Identify whethere we are stacking 2, 3 or 4 columns on Desktop
  if (document.querySelector('.content-container__item-stacker')) {
    setStackItemsSize();

    // start process
    optimizedResize.add(setStackItemsSize);
    /* END Resize Windows Handler */

    const siMediaQuery = window.matchMedia('(max-width: 736px)');
    stackItemsPageHandler(siMediaQuery); // Call listener function at run time
    siMediaQuery.addListener(stackItemsPageHandler); // Attach listener function on state changes

    /* Observing DOM Change to resize with contents */
    // Select the node that will be observed for mutations
    const targetNode = document.querySelector(
      '.content-container__item-stacker',
    );

    // Options for the observer (which mutations to observe)
    // const config = { attributes: false, childList: true, subtree: true };

    // Create an observer instance linked to the callback function
    // const observer = new MutationObserver(imageLoadObserver);

    // Start observing the target node for configured mutations
    // observer.observe(targetNode, config);

    imageLoadObserver();
  }
  initialised = true;
  console.log('Item Stacker Initialised');
}

console.log(document.readyState);
document.addEventListener('DOMContentLoaded', initItemStacker);
if (document.readyState !== 'loading' && !initialised) {
  initItemStacker();
}

module.exports = { initItemStacker, imageLoadObserver };
