import toggleHidden from './toggleHidden';
import itemViewer from './itemViewer';
import '../stylesheets/styles.scss';

function focusTrap(wrapper) {
  const focusable = wrapper.querySelectorAll(
    'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])',
  );
  const firstFocusable = focusable[0];
  const lastFocusable = focusable[focusable.length - 1];

  wrapper.addEventListener('keydown', e => {
    if (e.keyCode === 9) {
      // Rotate Focus
      if (e.shiftKey && document.activeElement === firstFocusable) {
        e.preventDefault();
        lastFocusable.focus();
      } else if (!e.shiftKey && document.activeElement === lastFocusable) {
        e.preventDefault();
        firstFocusable.focus();
      }
    }
  });
}

function handleSiteNavigation() {
  const sectionMenu = document.getElementsByClassName(
    'section-navigation__wrapper',
  );
  const siteMenu = document.querySelector('.main-navigation-menu__wrapper');
  const isHomePage = !!document.querySelector('.home-landing__contents');

  siteMenu.classList.toggle('main-navigation-menu__wrapper-active');
  document.body.classList.toggle('background-grey');

  if (sectionMenu.length > 0) {
    if (
      siteMenu.classList.contains('main-navigation-menu__wrapper-active') &&
      !sectionMenu[0].classList.contains('visibility-hidden') &&
      sectionMenu[0].classList.contains('js-section-navigation__mobile')
    ) {
      sectionMenu[0].classList.add('visibility-hidden');
    } else {
      sectionMenu[0].classList.remove('visibility-hidden');
    }

    if (!isHomePage) {
      document
        .querySelector('.header-container__navigation')
        .classList.toggle('col-md-10');
      document
        .querySelector('.header-container__navigation')
        .classList.toggle('col-md-2');
    }
  }

  const headerIcons = document.querySelectorAll(
    '.header-container__menu .header-container__icon',
  );

  headerIcons.forEach(el => {
    el.classList.toggle('js-hidden');
  });
}

document.addEventListener('DOMContentLoaded', () => {
  /* Hide All Empty Inner Content Container, prevent user errors */
  const allInnerContents = document.querySelectorAll(
    '.main-container__wrapper main > .content-container__item .content-container__inner',
  );

  allInnerContents.forEach(el => {
    if (el.innerText.length === 0) {
      const elImg = el.querySelector('img');
      if (elImg.getAttribute('src') === '') el.classList.add('hidden');
    }
  });

  /* TRIGGER MENU */
  document
    .querySelector('.header-container__menu')
    .addEventListener('click', handleSiteNavigation);
  /* HANDLE MENU HOVER */
  const navOptions = document.querySelectorAll(
    '.main-navigation-menu__options',
  );
  const mobileNav = document.getElementsByClassName(
    'section-navigation__menu',
  )[0];

  navOptions.forEach(option => {
    option.addEventListener('mouseenter', () => {
      const optionValue = option.getAttribute('data-menu-option').toLowerCase();
      const imageOptionValues = document.querySelector(
        `.main-navigation-menu__image[data-menu-option=${optionValue}]`,
      );

      imageOptionValues.classList.toggle(
        'js-main-navigation-menu__item-inactive',
      );
      toggleHidden(imageOptionValues);
    });
    option.addEventListener('mouseleave', () => {
      const optionValue = option.getAttribute('data-menu-option').toLowerCase();

      const imageOptionValue = document.querySelector(
        `.main-navigation-menu__image[data-menu-option=${optionValue}]`,
      );
      imageOptionValue.classList.toggle(
        'js-main-navigation-menu__item-inactive',
      );
      toggleHidden(imageOptionValue);
    });
  });

  if (mobileNav) {
    mobileNav.addEventListener('click', () => {
      mobileNav.classList.toggle('js-section-navigation__menu--expanded');
      mobileNav
        .querySelectorAll('.section-navigation__menu-icon')
        .forEach(icon => icon.classList.toggle('hidden'));
    });
  }

  focusTrap(document.querySelector('.main-navigation-menu__main-content'));

  function sectionNavScreenMode(mobileMediaQuery) {
    if (mobileMediaQuery.matches) {
      document
        .querySelector('.section-navigation__wrapper')
        .classList.remove('js-section-navigation__mobile');
    } else {
      document
        .querySelector('.section-navigation__wrapper')
        .classList.add('js-section-navigation__mobile');
    }
  }

  /**
   * Media Query with JS to convert the Section Navigation between Desktop and Mobile mode
   */
  if (document.querySelector('.section-navigation__wrapper')) {
    const mobileMediaQuery = window.matchMedia('(min-width: 768px)');

    sectionNavScreenMode(mobileMediaQuery);
    mobileMediaQuery.addListener(sectionNavScreenMode);
  }

  // toggle aria attribute
  const navButton = document.querySelector('.header-container__menu');
  navButton.addEventListener('click', () => {
    const expanded = navButton.getAttribute('aria-expanded') === 'true';
    navButton.setAttribute('aria-expanded', !expanded);
  });

  /**
   * Handle Splash Screen
   */
  if (!sessionStorage.getItem('mumaAcknowledgement')) {
    const splash = document.querySelector('.home-landing__splash-screen');
    if (splash) {
      splash.style.display = 'block';
      setTimeout(() => {
        document
          .querySelectorAll('.home-landing__splash-screen > *')
          .forEach(el => {
            el.style.opacity = '100'; // eslint-disable-line
          });
      }, 200);

      document
        .querySelector('.splash-screen__close')
        .addEventListener('click', () => {
          splash.style.display = 'none';
          sessionStorage.setItem('mumaAcknowledgement', '1');
        });
    }
  }

  if (!sessionStorage.getItem('deadlyLearningEntrance')) {
    const splash = document.querySelector('.dl-home-landing__splash-screen');
    if (splash) {
      const content = document.querySelector('.section-landing__contents');
      splash.style.display = 'block';
      content.style.display = 'none';
      setTimeout(() => {
        document
          .querySelectorAll('.dl-home-landing__splash-screen > *')
          .forEach(el => {
            el.style.opacity = '100'; // eslint-disable-line
          });
      }, 200);

      document
        .querySelector('.dl-splash-screen__close')
        .addEventListener('click', () => {
          splash.style.display = 'none';
          content.style.removeProperty('display');
          sessionStorage.setItem('deadlyLearningEntrance', '1');
        });
    }
  } else if (window.location.href.indexOf('/deadly-learning') < 0) {
    sessionStorage.removeItem('deadlyLearningEntrance');
  }
});

itemViewer();
