// Item viewer
function focusTrap(wrapper) {
  const focusable = wrapper.querySelectorAll(
    'button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])',
  );
  const firstFocusable = focusable[0];
  const lastFocusable = focusable[focusable.length - 1];

  wrapper.addEventListener('keydown', e => {
    if (e.keyCode === 9) {
      // Rotate Focus
      if (e.shiftKey && document.activeElement === firstFocusable) {
        e.preventDefault();
        lastFocusable.focus();
      } else if (!e.shiftKey && document.activeElement === lastFocusable) {
        e.preventDefault();
        firstFocusable.focus();
      }
    }
  });
}

function itemViewerWindowListener(event) {
  const itemViewerContainer = document.getElementById(
    'content-container__item-viewer',
  );
  const getCloseItem = document.getElementsByClassName('closeItemView');
  const getImageContainer = document.querySelector(
    '#content-container__item-viewer > .content-container__item-viewer--image',
  );
  if (event.keyCode === 27 && getImageContainer) {
    itemViewerContainer.remove();
  }
  if (event.keyCode === 9 && getImageContainer) {
    event.preventDefault();
    getCloseItem[0].focus();
  }
}
function itemViewer() {
  const getItem = document.querySelectorAll('.content-inner__image--expander');
  // const imageViewState = {
  //   open: false,
  // };

  getItem.forEach(item => {
    if (item.dataset.listenerActive === 'false') {
      item.dataset.listenerActive = 'true'; // eslint-disable-line no-param-reassign

      item.addEventListener('click', e => {
        e.preventDefault();
        document
          .querySelector('.section-navigation__wrapper')
          .classList.add('hidden');
        // Use the existing information on the page to render the text below the image
        const viewedItem = `<div data-simplebar id='content-container__item-viewer' aria-modal='true' tabindex='0'>
            <div class='content-container__item-viewer--image'>
              <img class='content-inner__image' src='${item.getAttribute(
                'href',
              )}' title='${item
          .getElementsByClassName('content-inner__image')[0]
          .getAttribute('title')}' alt='${item
          .getElementsByClassName('content-inner__image')[0]
          .getAttribute('alt')}' />
             
                <button class='closeItemView content-container__item-viewer--close'>
                 <svg title="close" class=''>
              <use xlink:href="https://www.monash.edu/__data/assets/git_bridge/0011/1572635/dist-assets/images/muma-icons.svg#close-dark"></use>
            </svg>
</button>
              </div>
              <div class='content-description__info'>${
                item
                  .closest('.content-container__inner')
                  .getElementsByClassName('content-description__info')[0]
                  .innerHTML
              }
            </div>
          </div>`;

        const getContentWrapper = document.getElementsByClassName(
          'main-container__wrapper',
        )[0];
        getContentWrapper.insertAdjacentHTML('afterbegin', viewedItem);
        const getCloseItem = document.querySelectorAll(
          '.content-container__item-viewer--close',
        );
        const getImageContainer = document.querySelector(
          '#content-container__item-viewer > .content-container__item-viewer--image',
        );
        const itemViewerContainer = document.getElementById(
          'content-container__item-viewer',
        );
        getImageContainer.focus();
        // eslint-disable-next-line
        focusTrap(document.querySelector('#content-container__item-viewer'));
        getCloseItem.forEach(items => {
          items.addEventListener('click', () => {
            document
              .querySelector('.section-navigation__wrapper')
              .classList.remove('hidden');
            itemViewerContainer.focus();
            itemViewerContainer.remove();
          });
        });
      });
    }
  });

  window.addEventListener('keydown', itemViewerWindowListener);
}

module.exports = itemViewer;
