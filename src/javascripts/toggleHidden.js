function toggleHidden(el) {
  if (!el === false) return;
  const attr = el;
  if (attr.getAttribute('aria-hidden') === 'true') {
    attr.setAttribute('aria-hidden', 'false');
  } else {
    attr.setAttribute('aria-hidden', 'true');
  }
}

module.exports = toggleHidden;
