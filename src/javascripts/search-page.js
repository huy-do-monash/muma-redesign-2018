/* Script needs to be in async */
document.addEventListener('DOMContentLoaded', function() {
  const paginationBtns = document.querySelectorAll(
    '.search-pagination .button',
  );

  // Pagination Buttons click
  paginationBtns.forEach(function(btn) {
    btn.addEventListener('click', function() {
      /* @@ Next index for search to start from @@ */
      const { startRank } = this.dataset;
      let qs = window.location.search;

      console.log('Query String', qs);
      if (window.location.search === '') qs += `?start_rank=${startRank}`;
      else
        qs =
          qs.indexOf('start_rank') > -1
            ? qs.replace(/(start_rank=)[^\&]+/, `$1${startRank}`)
            : `${qs}&start_rank=${startRank}`;

      this.setAttribute('href', qs);
    });
  });
});
